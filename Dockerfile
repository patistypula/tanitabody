FROM openjdk:14-alpine

RUN apk update && apk upgrade && \
    apk add git maven 

RUN mkdir /app

COPY ./ /app

EXPOSE 8080

WORKDIR /app

CMD java -jar /artifacts/test.jar